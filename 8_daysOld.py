# By Websten from forums
#
# Given your birthday and the current date, calculate your age in days.
# Account for leap days.
#
# Assume that the birthday and current date are correct dates (and no
# time travel).
#                  python 2.x

daysOfMonths = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

def daysBetweenDates(year1, month1, day1, year2, month2, day2):

    daysBetweenDates = 0
    yUpper = year2

    if year1 > year2 or (year2 == year1 and month1 > month2) or\
            (year2 == year1 and month2 == month1 and day1 > day2):
        return -1
    else:
        if year2 == year1:
            daysBetweenDates = daysSinceYearStart(year2, month2, day2) - daysSinceYearStart(year1, month1, day1)
            return daysBetweenDates
        else:
            daysBetweenDates = daysSinceYearStart(year1, 12, 31) - daysSinceYearStart(year1, month1, day1) +\
                daysSinceYearStart(year2, month2, day2)
            while (yUpper - 1) > year1:
                daysBetweenDates = daysBetweenDates + 365 + leapYear(yUpper - 1)
                yUpper = yUpper - 1
            return daysBetweenDates


def daysSinceYearStart(y, m, d):
    i = 0
    daysSinceJanFirst = 0

    while m >= i + 1:
        if m > i + 1:
            daysSinceJanFirst = daysSinceJanFirst + daysOfMonths[i]
        elif m == i + 1:
            daysSinceJanFirst = daysSinceJanFirst + d
        i = i + 1

    if m > 2 and leapYear(y):
        return daysSinceJanFirst + 1
    else:
        return daysSinceJanFirst


def leapYear(year):
    if year % 4 != 0:
        return False            # common year
    elif year % 100 != 0:
        return True             # leap year
    elif year % 400 != 0:
        return False            # common year
    else:
        return True             # leap year


# Test routine

def test():

    test_cases = [((2012,1,1,2012,2,28), 58),
                  ((2012,1,1,2012,3,1), 60),
                  ((2011,6,30,2012,6,30), 366),
                  ((2011,1,1,2012,8,8), 585 ),
                  ((1900,1,1,1999,12,31), 36523)]
    for (args, answer) in test_cases:
        result = daysBetweenDates(*args)
        if result != answer:
            print "Test with data:", args, "failed"
        else:
            print "Test case passed!"

test()
