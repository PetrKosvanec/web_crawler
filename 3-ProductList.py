# Define a procedure, product_list,
# that takes as input a list of numbers,
# and returns a number that is
# the result of multiplying all
# those numbers together.

def product_list(list_of_numbers):

    numbersInList = True
    if list_of_numbers == []:
        return 1
    for n in list_of_numbers:
        if (isinstance(n, (int, long, float)) == False):
            numbersInList = False

    if numbersInList == False:
        return -1
    else:
        product = 1
        for n in list_of_numbers:
            product = product * n
        return product







print product_list([9])
#>>> 9

print product_list([1,2,3,4])
#>>> 24

print product_list([])
#>>> 1

print product_list([0.5, 0.5])