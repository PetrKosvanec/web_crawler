# Define a procedure, greatest,
# that takes as input a list
# of positive numbers, and
# returns the greatest number
# in that list. If the input
# list is empty, the output
# should be 0.

def greatest(list_of_numbers):
    numbersInList = True
    if list_of_numbers == []:
        return 0
    else:
        greatestN = list_of_numbers[0]
        for n in list_of_numbers:
            if (isinstance(n, (int, long, float)) == False) or n < 0:
                return -1
            elif n > greatestN:
                greatestN = n
        return greatestN









print greatest([4,23,1])
#>>> 23
print greatest([])
#>>> 0
print greatest([1,-5])

print greatest([1,'g',-5])
